package com.ukan.loc.entity;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class UtilModel {
	/**
	 *  I saw this on the tutorial of Mowzie this might help us in the future work of better animation with easy understanding.
	 * 
	 * @param box
	 * @param speed
	 * @param degree
	 * @param invert
	 * @param offset
	 * @param weight
	 * @param limbSwing
	 * @param limbSwingAmount
	 */
	
    public void bob(ModelRenderer box, float speed, float degree, boolean invert, float offset, float weight, float limbSwing, float limbSwingAmount) {
        float bob =(float) (Math.sin(limbSwing * speed) * limbSwing * degree - limbSwingAmount * degree);
        if(invert) bob = (float)-Math.abs((Math.sin(limbSwing * speed) * limbSwingAmount * degree));
    	box.rotationPointY = bob;
    }

    public void walk(ModelRenderer box, float speed, float degree, boolean invert, float offset, float weight, float limbSwing, float limbSwingAmount) {
        box.rotateAngleX = this.computeAnimation(speed, degree, invert, offset, weight, limbSwing, limbSwingAmount);
    }

    public void swing(ModelRenderer box, float speed, float degree, boolean invert, float offset, float weight, float limbSwing, float limbSwingAmount) {
        box.rotateAngleY = this.computeAnimation(speed, degree, invert, offset, weight, limbSwing, limbSwingAmount);
    }

    public void flap(ModelRenderer box, float speed, float degree, boolean invert, float offset, float weight, float limbSwing, float limbSwingAmount) {
        box.rotateAngleZ = this.computeAnimation(speed, degree, invert, offset, weight, limbSwing, limbSwingAmount);
    }

    public void chainSwing(ModelRenderer[] boxes, float speed, float degree, boolean invert, double rootOffset, float swing, float swingAmount) {
        float offset = this.computeChainOffset(boxes, rootOffset);
        for (int index = 0; index < boxes.length; index++) {
            boxes[index].rotateAngleY = this.computeChainRotation(speed, degree, invert, offset, index, swing, swingAmount);
        }
    }

    public void chainWave(ModelRenderer[] boxes, float speed, float degree, boolean invert, double rootOffset, float swing, float swingAmount) {
        float offset = this.computeChainOffset(boxes, rootOffset);
        for (int index = 0; index < boxes.length; index++) {
            boxes[index].rotateAngleX = this.computeChainRotation(speed, degree, invert, offset, index, swing, swingAmount);
        }
    }

    public void chainFlap(ModelRenderer[] boxes, float speed, float degree, boolean invert, double rootOffset, float swing, float swingAmount) {
        float offset = this.computeChainOffset(boxes, rootOffset);
        for (int index = 0; index < boxes.length; index++) {
            boxes[index].rotateAngleZ = this.computeChainRotation(speed, degree, invert, offset, index, swing, swingAmount);
        }
    }

    public float computeChainRotation(float speed, float degree, boolean invert, float offset, int boxIndex, float swing, float swingAmount) {
        float theta = swing * speed + offset * boxIndex;
        float rotation = MathHelper.cos(theta) * swingAmount * degree;
        return invert ? -rotation : rotation;
    }

    public float computeChainOffset(ModelRenderer[] boxes, double rootOffset) {
        return (float) (rootOffset * Math.PI / (2 * boxes.length));
    }

    public float computeAnimation(float speed, float degree, boolean invert, float offset, float weight, float limbSwing, float limbSwingAmount) {
        float theta = limbSwing * speed + offset;
        float scaledWeight = weight * limbSwingAmount;
        float rotation = (MathHelper.cos(theta) * degree * limbSwingAmount) + scaledWeight;
        return invert ? -rotation : rotation;
    }

}
