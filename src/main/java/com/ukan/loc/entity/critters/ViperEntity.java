package com.ukan.loc.entity.critters;

import javax.annotation.Nullable;

import com.ukan.loc.LOCEntities;
import com.ukan.loc.entity.CritterEntity;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.TemptGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class ViperEntity extends CritterEntity {
	
	protected static DataParameter<Integer> TYPE = EntityDataManager.createKey(ViperEntity.class, DataSerializers.VARINT);

	public ViperEntity(EntityType<? extends ViperEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	@Override
	public void registerGoals() {
		// Basic AIs Available for Minecraft.
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 2.0D));
		this.goalSelector.addGoal(3, new TemptGoal(this, 1.25D, Ingredient.fromItems(Items.WHEAT), false));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 6.0F));
		this.goalSelector.addGoal(7, new LookRandomlyGoal(this));
		super.registerGoals();
	}

	@Override
	public void registerAttributes() {
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(8D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.2F);
		
	}
	
	@Override
	public boolean processInteract(PlayerEntity player, Hand hand) {
		// Variable that checks what ItemStack is in a players hand
		ItemStack itemStack =  player.getHeldItem(hand);
		if(itemStack.isEmpty() && !player.abilities.isCreativeMode) {
			player.attackEntityFrom(DamageSource.causeMobDamage(this), 3f);
		}
		return super.processInteract(player, hand);
	}
	
	@Override
	protected void registerData() {
		super.registerData();
		this.dataManager.register(TYPE, 0);
	}
	
	public int getBool() {
		return this.dataManager.get(TYPE);
	}
	
	public void setColor(int bool) {
		this.dataManager.set(TYPE, bool);
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		super.notifyDataManagerChange(key);
	}
	
	@Override
	public void writeAdditional(CompoundNBT compound) {
		compound.putInt("TYPE", this.getBool());
		super.writeAdditional(compound);
	}
	
	@Override
	public void readAdditional(CompoundNBT compound) {
		this.setColor(getBool());
		super.readAdditional(compound);
	}

	
	@Override
	public AgeableEntity createChild(AgeableEntity ageable) {
		ViperEntity ViperEntity = LOCEntities.viper_entity.create(this.world);
		int i = getColubridType();
		if (this.rand.nextInt(20) != 0) {
			 if (ageable instanceof ViperEntity && this.rand.nextBoolean()) {
				 i = ((ViperEntity)ageable).getBool();
			 }
			 else {
		            i = this.getBool();
		         }
		}
		ViperEntity.setColor(i);
		return ViperEntity;
	}
	
	@Nullable
	public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		int i = this.getColubridType();
		 if (spawnDataIn instanceof ViperEntity.ViperData) {
	         i = ((ViperEntity.ViperData)spawnDataIn).typeData;
	      } else {
	         spawnDataIn = new ViperEntity.ViperData(i);
	      }

	      this.setColor(i);
	      return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	   
	}
	private int getColubridType() {
		return this.rand.nextInt(10);
	}
	
	public static class ViperData extends AgeableEntity.AgeableData{
		public final int typeData;
		
		public ViperData(int type) {
			this.typeData = type;
			// Spawn probablity of a baby - func_226258.
			this.func_226258_a_(1.0F);
		}
	}
	
}
