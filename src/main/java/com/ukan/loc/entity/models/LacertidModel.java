package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.LacertidEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCLacertid - UkanGundun
 * Created using Tabula 8.0.0
 */
@OnlyIn(Dist.CLIENT)
public class LacertidModel extends EntityModel<LacertidEntity> {
    public ModelRenderer body;
    public ModelRenderer neck;
    public ModelRenderer tail1;
    public ModelRenderer armL;
    public ModelRenderer armR;
    public ModelRenderer legL;
    public ModelRenderer legR;
    public ModelRenderer head;
    public ModelRenderer tail2;
    public ModelRenderer tail3;

    public LacertidModel() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.tail1 = new ModelRenderer(this, 0, 10);
        this.tail1.setRotationPoint(0.0F, 0.0F, 2.5F);
        this.tail1.addBox(-0.5F, -1.0F, 0.0F, 1.0F, 2.0F, 5.0F, 0.3F, -0.2F, 0.0F);
        this.setRotateAngle(tail1, 0.07888888185724856F, 0.0F, 0.0F);
        this.armR = new ModelRenderer(this, 20, 10);
        this.armR.setRotationPoint(-0.5F, 0.5F, -2.0F);
        this.armR.addBox(-1.0F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(armR, -0.512777719588548F, 0.0F, 0.7888888352172424F);
        this.legL = new ModelRenderer(this, 20, 14);
        this.legL.setRotationPoint(0.5F, 0.5F, 3.0F);
        this.legL.addBox(0.0F, 0.0F, -0.5F, 1.0F, 2.5F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(legL, 0.512777719588548F, 0.0F, -1.3411109333165765F);
        this.legR = new ModelRenderer(this, 20, 14);
        this.legR.mirror = true;
        this.legR.setRotationPoint(-0.5F, 0.5F, 3.0F);
        this.legR.addBox(-1.0F, 0.0F, -0.5F, 1.0F, 2.5F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(legR, 0.512777719588548F, 0.0F, 1.3411109333165765F);
        this.body = new ModelRenderer(this, 0, 0);
        this.body.setRotationPoint(0.0F, 22.6F, 0.0F);
        this.body.addBox(-1.0F, -1.0F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(body, -0.11833332694706204F, 0.0F, 0.0F);
        this.neck = new ModelRenderer(this, 20, 0);
        this.neck.setRotationPoint(0.0F, 0.0F, -2.0F);
        this.neck.addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, -0.2F, -0.2F, 0.0F);
        this.setRotateAngle(neck, -0.4338888626984347F, 0.0F, 0.0F);
        this.head = new ModelRenderer(this, 30, 0);
        this.head.setRotationPoint(0.0F, -0.2F, -1.6F);
        this.head.addBox(-1.0F, -1.0F, -3.0F, 2.0F, 2.0F, 3.0F, 0.0F, -0.2F, 0.0F);
        this.setRotateAngle(head, 0.4340633996183757F, 0.0F, 0.0F);
        this.tail2 = new ModelRenderer(this, 0, 20);
        this.tail2.setRotationPoint(0.0F, 0.0F, 4.5F);
        this.tail2.addBox(-0.5F, -1.0F, 0.0F, 1.0F, 2.0F, 5.0F, 0.1F, -0.4F, 0.0F);
        this.tail3 = new ModelRenderer(this, 16, 20);
        this.tail3.setRotationPoint(0.0F, 0.0F, 4.5F);
        this.tail3.addBox(-0.5F, -1.0F, 0.0F, 1.0F, 2.0F, 5.0F, 0.0F, -0.5F, 0.0F);
        this.armL = new ModelRenderer(this, 20, 10);
        this.armL.setRotationPoint(0.5F, 0.5F, -2.0F);
        this.armL.addBox(0.0F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(armL, -0.512777719588548F, 0.0F, -0.7888888352172424F);
        this.body.addChild(this.tail1);
        this.body.addChild(this.armR);
        this.body.addChild(this.legL);
        this.body.addChild(this.legR);
        this.body.addChild(this.neck);
        this.neck.addChild(this.head);
        this.tail1.addChild(this.tail2);
        this.tail2.addChild(this.tail3);
        this.body.addChild(this.armL);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.body).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    @Override
    public void setRotationAngles(LacertidEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {}

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
