package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.PhrogeEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCPhroge - UkanGundun
 * Created using Tabula 8.0.0
 */
@OnlyIn(Dist.CLIENT)
public class PhrogeModel extends EntityModel<PhrogeEntity> {
    public ModelRenderer body;
    public ModelRenderer head;
    public ModelRenderer frontlegL;
    public ModelRenderer frontlegR;
    public ModelRenderer backleg1L;
    public ModelRenderer backleg1R;
    public ModelRenderer faceR;
    public ModelRenderer faceL;
    public ModelRenderer eyeL;
    public ModelRenderer eyeR;
    public ModelRenderer bubble;
    public ModelRenderer backleg2L;
    public ModelRenderer footL;
    public ModelRenderer backleg2R;
    public ModelRenderer footR;

    public PhrogeModel() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.faceR = new ModelRenderer(this, 0, 10);
        this.faceR.setRotationPoint(-1.0F, 0.0F, 0.0F);
        this.faceR.addBox(-0.5F, -0.5F, -3.0F, 1.0F, 1.5F, 3.0F, 0.2F, 0.09F, 0.0F);
        this.setRotateAngle(faceR, 0.0F, -0.1972222088043106F, 0.0F);
        this.bubble = new ModelRenderer(this, 26, 0);
        this.bubble.setRotationPoint(0.0F, 2.0F, -1.5F);
        this.bubble.addBox(-1.5F, -1.0F, -2.0F, 3.0F, 2.0F, 4.0F, 0.0F, 0.0F, -0.3F);
        this.head = new ModelRenderer(this, 0, 7);
        this.head.setRotationPoint(0.0F, -0.5F, 0.3F);
        this.head.addBox(-1.0F, -0.5F, -2.0F, 2.0F, 1.0F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(head, 0.15777776371449712F, 0.07888888185724856F, 0.0F);
        this.body = new ModelRenderer(this, 0, 0);
        this.body.setRotationPoint(0.0F, 21.5F, 0.0F);
        this.body.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 4.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(body, -0.15777776371449712F, 0.0F, 0.0F);
        this.eyeR = new ModelRenderer(this, 10, 7);
        this.eyeR.mirror = true;
        this.eyeR.setRotationPoint(-1.0F, -0.4F, -1.5F);
        this.eyeR.addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(eyeR, 0.0F, -0.23666665389412408F, 0.43039819087864056F);
        this.backleg2L = new ModelRenderer(this, 15, 3);
        this.backleg2L.setRotationPoint(3.0F, 0.0F, 1.0F);
        this.backleg2L.addBox(-3.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, -0.3F, 0.0F, -0.3F);
        this.setRotateAngle(backleg2L, 0.15777776371449712F, 0.31555552742899423F, -0.512777719588548F);
        this.backleg1L = new ModelRenderer(this, 15, 0);
        this.backleg1L.setRotationPoint(0.7F, 0.0F, 3.0F);
        this.backleg1L.addBox(0.0F, -0.5F, 0.0F, 3.0F, 1.0F, 2.0F, -0.3F, 0.0F, -0.2F);
        this.setRotateAngle(backleg1L, -0.15777776371449712F, 0.8677777253968694F, 0.11833332694706204F);
        this.frontlegR = new ModelRenderer(this, 10, 10);
        this.frontlegR.setRotationPoint(-1.0F, 0.0F, 0.5F);
        this.frontlegR.addBox(-1.0F, 0.0F, -0.5F, 1.0F, 2.5F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(frontlegR, 0.15777776371449712F, -0.47333330778824817F, 0.0F);
        this.footL = new ModelRenderer(this, 15, 6);
        this.footL.mirror = true;
        this.footL.setRotationPoint(-2.9F, 0.2F, 0.0F);
        this.footL.addBox(0.0F, 0.0F, -1.0F, 3.0F, 0.0F, 2.0F, 0.0F, 0.0F, -0.3F);
        this.setRotateAngle(footL, 0.03944444092862428F, 0.0F, 0.6311110548579885F);
        this.footR = new ModelRenderer(this, 15, 6);
        this.footR.setRotationPoint(2.9F, 0.2F, 0.0F);
        this.footR.addBox(-3.0F, 0.0F, -1.0F, 3.0F, 0.0F, 2.0F, 0.0F, 0.0F, -0.3F);
        this.setRotateAngle(footR, 0.03944444092862428F, 0.0F, -0.6311110548579885F);
        this.backleg1R = new ModelRenderer(this, 15, 0);
        this.backleg1R.setRotationPoint(-0.7F, 0.0F, 3.0F);
        this.backleg1R.addBox(-3.0F, -0.5F, 0.0F, 3.0F, 1.0F, 2.0F, -0.3F, 0.0F, -0.2F);
        this.setRotateAngle(backleg1R, -0.15777776371449712F, -0.9466666155764963F, -0.11833332694706204F);
        this.backleg2R = new ModelRenderer(this, 15, 3);
        this.backleg2R.setRotationPoint(-3.0F, 0.0F, 1.0F);
        this.backleg2R.addBox(0.0F, -0.5F, -1.0F, 3.0F, 1.0F, 2.0F, -0.3F, 0.0F, -0.3F);
        this.setRotateAngle(backleg2R, 0.15777776371449712F, -0.31555552742899423F, 0.512777719588548F);
        this.frontlegL = new ModelRenderer(this, 10, 10);
        this.frontlegL.setRotationPoint(1.0F, 0.0F, 0.5F);
        this.frontlegL.addBox(0.0F, 0.0F, -0.5F, 1.0F, 2.5F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(frontlegL, 0.15777776371449712F, 0.47333330778824817F, 0.0F);
        this.eyeL = new ModelRenderer(this, 10, 7);
        this.eyeL.setRotationPoint(1.0F, -0.4F, -1.5F);
        this.eyeL.addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(eyeL, 0.0F, 0.23666665389412408F, -0.43039819087864056F);
        this.faceL = new ModelRenderer(this, 0, 10);
        this.faceL.setRotationPoint(1.0F, 0.0F, 0.0F);
        this.faceL.addBox(-0.5F, -0.5F, -3.0F, 1.0F, 1.5F, 3.0F, 0.2F, 0.1F, 0.0F);
        this.setRotateAngle(faceL, 0.0F, 0.1972222088043106F, 0.0F);
        this.head.addChild(this.faceR);
        this.head.addChild(this.bubble);
        this.body.addChild(this.head);
        this.head.addChild(this.eyeR);
        this.backleg1L.addChild(this.backleg2L);
        this.body.addChild(this.backleg1L);
        this.body.addChild(this.frontlegR);
        this.backleg2L.addChild(this.footL);
        this.backleg2R.addChild(this.footR);
        this.body.addChild(this.backleg1R);
        this.backleg1R.addChild(this.backleg2R);
        this.body.addChild(this.frontlegL);
        this.head.addChild(this.eyeL);
        this.head.addChild(this.faceL);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.body).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    @Override
    public void setRotationAngles(PhrogeEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {}

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
