package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.ViperEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCViper - UkanGundun
 * Created using Tabula 8.0.0
 */

@OnlyIn(Dist.CLIENT)
public class ViperModel extends EntityModel<ViperEntity> {
    public ModelRenderer body12;
    public ModelRenderer body11;
    public ModelRenderer body10;
    public ModelRenderer body9;
    public ModelRenderer body8;
    public ModelRenderer body7;
    public ModelRenderer body6;
    public ModelRenderer body5;
    public ModelRenderer body4;
    public ModelRenderer body3;
    public ModelRenderer body2;
    public ModelRenderer body1;
    public ModelRenderer head;
    public ModelRenderer tongue;
    public ModelRenderer headL;
    public ModelRenderer headR;
    public ModelRenderer horns;

    public ViperModel() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.body11 = new ModelRenderer(this, 60, 0);
        this.body11.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body11.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.5F, -0.4F, 0.0F);
        this.horns = new ModelRenderer(this, 20, 10);
        this.horns.setRotationPoint(0.0F, -1.0F, 1.0F);
        this.horns.addBox(-1.5F, -0.5F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(horns, 0.7853981633974483F, 0.0F, 0.0F);
        this.body6 = new ModelRenderer(this, 40, 0);
        this.body6.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body6.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.0F, 0.1F, 0.0F);
        this.body12 = new ModelRenderer(this, 60, 0);
        this.body12.setRotationPoint(0.0F, 23.0F, 25.0F);
        this.body12.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.6F, -0.5F, 0.0F);
        this.body5 = new ModelRenderer(this, 40, 0);
        this.body5.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body5.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.01F, 0.11F, 0.0F);
        this.body2 = new ModelRenderer(this, 40, 0);
        this.body2.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body2.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.2F, -0.09F, 0.0F);
        this.body8 = new ModelRenderer(this, 40, 0);
        this.body8.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body8.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.2F, -0.1F, 0.0F);
        this.headR = new ModelRenderer(this, 0, 10);
        this.headR.mirror = true;
        this.headR.setRotationPoint(0.0F, 0.0F, 1.7F);
        this.headR.addBox(-2.0F, -1.0F, -4.0F, 2.0F, 2.0F, 4.0F, 0.0F, -0.01F, 0.0F);
        this.setRotateAngle(headR, 0.0F, -0.15777776371449712F, 0.0F);
        this.body9 = new ModelRenderer(this, 40, 0);
        this.body9.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body9.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.3F, -0.2F, 0.0F);
        this.body4 = new ModelRenderer(this, 40, 0);
        this.body4.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body4.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.0F, 0.1F, 0.0F);
        this.body7 = new ModelRenderer(this, 40, 0);
        this.body7.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body7.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.1F, 0.0F, 0.0F);
        this.headL = new ModelRenderer(this, 0, 10);
        this.headL.setRotationPoint(0.0F, 0.0F, 1.7F);
        this.headL.addBox(0.0F, -1.0F, -4.0F, 2.0F, 2.0F, 4.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(headL, 0.0F, 0.15777776371449712F, 0.0F);
        this.body3 = new ModelRenderer(this, 40, 0);
        this.body3.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body3.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.1F, 0.0F, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.head.addBox(-1.5F, -1.0F, -1.5F, 3.0F, 2.0F, 3.0F, 0.0F, -0.2F, 0.0F);
        this.tongue = new ModelRenderer(this, 10, 10);
        this.tongue.setRotationPoint(0.0F, 0.5F, -2.0F);
        this.tongue.addBox(-0.5F, 0.0F, -2.0F, 1.0F, 0.0F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.body1 = new ModelRenderer(this, 20, 0);
        this.body1.setRotationPoint(0.0F, 0.0F, -4.0F);
        this.body1.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.3F, -0.1F, 0.0F);
        this.body10 = new ModelRenderer(this, 40, 0);
        this.body10.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body10.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.4F, -0.3F, 0.0F);
        this.body12.addChild(this.body11);
        this.head.addChild(this.horns);
        this.body7.addChild(this.body6);
        this.body6.addChild(this.body5);
        this.body3.addChild(this.body2);
        this.body9.addChild(this.body8);
        this.head.addChild(this.headR);
        this.body10.addChild(this.body9);
        this.body5.addChild(this.body4);
        this.body8.addChild(this.body7);
        this.head.addChild(this.headL);
        this.body4.addChild(this.body3);
        this.body1.addChild(this.head);
        this.head.addChild(this.tongue);
        this.body2.addChild(this.body1);
        this.body11.addChild(this.body10);
    }
    
    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.body12).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

	@Override
	public void setRotationAngles(ViperEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		if(entityIn.getAttackTarget() != null || entityIn.getRNG().nextInt(100) == 0) {
			this.tongue.showModel = true;
		}else {
			this.tongue.showModel = false;
		}
		ModelRenderer[]	 bodySegment = new ModelRenderer[] {body12,body11,body10,body9,body8,body7,body6,body5,body4,body3,body2,body1};
		for(int i = 0; i < bodySegment.length; ++i) {
			bodySegment[i].rotateAngleY = MathHelper.cos(limbSwing * 0.9f + (float)i * 0.15f * (float)Math.PI ) * (float) Math.PI * 0.01f* (float)(1 + Math.abs(i - 2)) * (float)Math.PI * 0.12f;
		}
	}
}
