package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.LeglessLizardEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCLegless_lizard - UkanGundun
 * Created using Tabula 8.0.0
 */
@OnlyIn(Dist.CLIENT)
public class LeglessLizardModel extends EntityModel<LeglessLizardEntity> {
    public ModelRenderer head;
    public ModelRenderer body1;
    public ModelRenderer body2;
    public ModelRenderer body3;
    public ModelRenderer body4;
    public ModelRenderer body5;

    public LeglessLizardModel() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.body2 = new ModelRenderer(this, 10, 0);
        this.body2.setRotationPoint(0.0F, 0.0F, 5.5F);
        this.body2.addBox(-1.0F, -1.0F, 0.0F, 2.0F, 2.0F, 6.0F, -0.15F, -0.15F, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.0F, 23.0F, 0.0F);
        this.head.addBox(-1.0F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, 0.0F, 0.0F);
        this.body4 = new ModelRenderer(this, 10, 0);
        this.body4.setRotationPoint(0.0F, 0.0F, 5.5F);
        this.body4.addBox(-1.0F, -1.0F, 0.0F, 2.0F, 2.0F, 6.0F, -0.35F, -0.35F, 0.0F);
        this.body1 = new ModelRenderer(this, 10, 0);
        this.body1.setRotationPoint(0.0F, 0.0F, 1.0F);
        this.body1.addBox(-1.0F, -1.0F, 0.0F, 2.0F, 2.0F, 6.0F, -0.1F, -0.1F, 0.0F);
        this.body5 = new ModelRenderer(this, 10, 0);
        this.body5.setRotationPoint(0.0F, 0.0F, 5.5F);
        this.body5.addBox(-1.0F, -1.0F, 0.0F, 2.0F, 2.0F, 6.0F, -0.45F, -0.45F, 0.0F);
        this.body3 = new ModelRenderer(this, 10, 0);
        this.body3.setRotationPoint(0.0F, 0.0F, 5.5F);
        this.body3.addBox(-1.0F, -1.0F, 0.0F, 2.0F, 2.0F, 6.0F, -0.25F, -0.25F, 0.0F);
        this.body1.addChild(this.body2);
        this.body3.addChild(this.body4);
        this.head.addChild(this.body1);
        this.body4.addChild(this.body5);
        this.body2.addChild(this.body3);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.head).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    @Override
    public void setRotationAngles(LeglessLizardEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {}

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
