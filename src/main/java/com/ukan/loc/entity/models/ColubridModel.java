package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.ColubridEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCColubrid - UkanGundun
 * Created using Tabula 8.0.0
 */
@OnlyIn(Dist.CLIENT)
public class ColubridModel extends EntityModel<ColubridEntity> {
    public ModelRenderer body20;
    public ModelRenderer body19;
    public ModelRenderer body18;
    public ModelRenderer body17;
    public ModelRenderer body16;
    public ModelRenderer body15;
    public ModelRenderer body14;
    public ModelRenderer body13;
    public ModelRenderer body12;
    public ModelRenderer body11;
    public ModelRenderer body10;
    public ModelRenderer body9;
    public ModelRenderer body8;
    public ModelRenderer body7;
    public ModelRenderer body6;
    public ModelRenderer body5;
    public ModelRenderer body4;
    public ModelRenderer body3;
    public ModelRenderer body2;
    public ModelRenderer body1;
    public ModelRenderer head;
    public ModelRenderer tongue;

    public ColubridModel() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.body18 = new ModelRenderer(this, 80, 0);
        this.body18.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body18.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.41F, -0.31F, 0.0F);
        this.body12 = new ModelRenderer(this, 40, 0);
        this.body12.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body12.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.11F, -0.01F, 0.0F);
        this.body4 = new ModelRenderer(this, 40, 0);
        this.body4.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body4.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.0F, 0.1F, 0.0F);
        this.body6 = new ModelRenderer(this, 40, 0);
        this.body6.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body6.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.02F, 0.12F, 0.0F);
        this.body9 = new ModelRenderer(this, 40, 0);
        this.body9.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body9.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.01F, 0.11F, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.0F, 0.0F, -2.0F);
        this.head.addBox(-1.5F, -1.0F, -2.5F, 3.0F, 2.0F, 5.0F, 0.0F, 0.0F, 0.0F);
        this.body17 = new ModelRenderer(this, 80, 0);
        this.body17.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body17.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.4F, -0.3F, 0.0F);
        this.body14 = new ModelRenderer(this, 80, 0);
        this.body14.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body14.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.21F, -0.11F, 0.0F);
        this.body10 = new ModelRenderer(this, 40, 0);
        this.body10.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body10.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.0F, 0.1F, 0.0F);
        this.body5 = new ModelRenderer(this, 40, 0);
        this.body5.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body5.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.01F, 0.11F, 0.0F);
        this.tongue = new ModelRenderer(this, 0, 0);
        this.tongue.setRotationPoint(0.0F, 0.5F, -2.0F);
        this.tongue.addBox(-0.5F, 0.0F, -3.0F, 1.0F, 0.0F, 3.0F, 0.0F, 0.0F, 0.0F);
        this.body3 = new ModelRenderer(this, 40, 0);
        this.body3.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body3.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.1F, 0.0F, 0.0F);
        this.body1 = new ModelRenderer(this, 20, 0);
        this.body1.setRotationPoint(0.0F, 0.0F, -4.0F);
        this.body1.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.3F, -0.1F, 0.0F);
        this.body19 = new ModelRenderer(this, 80, 0);
        this.body19.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body19.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.5F, -0.4F, 0.0F);
        this.body2 = new ModelRenderer(this, 40, 0);
        this.body2.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body2.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.2F, -0.09F, 0.0F);
        this.body8 = new ModelRenderer(this, 40, 0);
        this.body8.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body8.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.02F, 0.12F, 0.0F);
        this.body20 = new ModelRenderer(this, 80, 0);
        this.body20.setRotationPoint(0.0F, 23.0F, 45.0F);
        this.body20.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.51F, -0.41F, 0.0F);
        this.body15 = new ModelRenderer(this, 80, 0);
        this.body15.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body15.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.3F, -0.2F, 0.0F);
        this.body16 = new ModelRenderer(this, 80, 0);
        this.body16.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body16.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.31F, -0.21F, 0.0F);
        this.body13 = new ModelRenderer(this, 60, 0);
        this.body13.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body13.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.2F, -0.1F, 0.0F);
        this.body11 = new ModelRenderer(this, 40, 0);
        this.body11.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body11.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, -0.1F, 0.0F, 0.0F);
        this.body7 = new ModelRenderer(this, 40, 0);
        this.body7.setRotationPoint(0.0F, 0.0F, -4.5F);
        this.body7.addBox(-1.5F, -1.0F, 0.0F, 3.0F, 2.0F, 5.0F, 0.03F, 0.13F, 0.0F);
        this.body19.addChild(this.body18);
        this.body13.addChild(this.body12);
        this.body5.addChild(this.body4);
        this.body7.addChild(this.body6);
        this.body10.addChild(this.body9);
        this.body1.addChild(this.head);
        this.body18.addChild(this.body17);
        this.body15.addChild(this.body14);
        this.body11.addChild(this.body10);
        this.body6.addChild(this.body5);
        this.head.addChild(this.tongue);
        this.body4.addChild(this.body3);
        this.body2.addChild(this.body1);
        this.body20.addChild(this.body19);
        this.body3.addChild(this.body2);
        this.body9.addChild(this.body8);
        this.body16.addChild(this.body15);
        this.body17.addChild(this.body16);
        this.body14.addChild(this.body13);
        this.body12.addChild(this.body11);
        this.body8.addChild(this.body7);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.body20).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    @Override
    public void setRotationAngles(ColubridEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
    	if(entityIn.getAttackTarget() != null || entityIn.getRNG().nextInt(100) == 0) {
    		this.tongue.showModel = true;
    	}else {
    		this.tongue.showModel = false;
    	}
    	ModelRenderer[] bodySegment = new ModelRenderer[] {body20, body19,body18,body17,body16,body15,body14,body13,body12,body11,body10,body9,body8,body7,body6,body5,body4,body3,body2,body1};
    	 for(int i = 0; i < bodySegment.length; ++i) {
    		 bodySegment[i].rotateAngleY = MathHelper.cos(limbSwing * 0.9F + (float)i * 0.15F * (float)Math.PI) * (float)Math.PI * 0.01F * (float)(1 + Math.abs(i - 2)) * (float)Math.PI * 0.12f;
    	    	    
    	 }
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
