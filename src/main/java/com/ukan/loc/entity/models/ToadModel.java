package com.ukan.loc.entity.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.ukan.loc.entity.critters.ToadEntity;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * LoCToad - UkanGundun
 * Created using Tabula 8.0.0
 */
@OnlyIn(Dist.CLIENT)
public class ToadModel extends EntityModel<ToadEntity> {
    public ModelRenderer body;
    public ModelRenderer head;
    public ModelRenderer frontlegL;
    public ModelRenderer backleg1L;
    public ModelRenderer backleg1R;
    public ModelRenderer frontlegR;
    public ModelRenderer bubble;
    public ModelRenderer eyeL;
    public ModelRenderer eyeR;
    public ModelRenderer backleg2L;
    public ModelRenderer backleg2R;

    public ToadModel() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.backleg2L = new ModelRenderer(this, 22, 10);
        this.backleg2L.setRotationPoint(3.0F, 0.0F, -1.0F);
        this.backleg2L.addBox(0.0F, -0.5F, 0.0F, 3.0F, 1.0F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(backleg2L, -0.3944444176086212F, -1.064650843716541F, 0.07888888185724856F);
        this.head = new ModelRenderer(this, 20, 0);
        this.head.setRotationPoint(0.0F, -0.5F, -2.0F);
        this.head.addBox(-2.0F, -1.0F, -4.0F, 4.0F, 2.0F, 4.0F, -0.1F, 0.0F, 0.0F);
        this.setRotateAngle(head, 0.4743804960183809F, 0.0F, 0.0F);
        this.frontlegR = new ModelRenderer(this, 0, 10);
        this.frontlegR.setRotationPoint(-1.5F, 1.0F, -1.4F);
        this.frontlegR.addBox(-1.0F, 0.0F, -1.0F, 1.0F, 3.5F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(frontlegR, 0.3549999725188077F, -0.47333330778824817F, 0.15777776371449712F);
        this.eyeR = new ModelRenderer(this, 10, 15);
        this.eyeR.mirror = true;
        this.eyeR.setRotationPoint(-1.6F, -0.9F, -2.4F);
        this.eyeR.addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(eyeR, 0.3549999725188077F, -0.1972222088043106F, 0.43039819087864056F);
        this.frontlegL = new ModelRenderer(this, 0, 10);
        this.frontlegL.setRotationPoint(1.5F, 1.0F, -1.4F);
        this.frontlegL.addBox(0.0F, 0.0F, -1.0F, 1.0F, 3.5F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(frontlegL, 0.3549999725188077F, 0.47333330778824817F, -0.15777776371449712F);
        this.backleg2R = new ModelRenderer(this, 22, 10);
        this.backleg2R.setRotationPoint(-3.0F, 0.0F, -1.0F);
        this.backleg2R.addBox(-3.0F, -0.5F, 0.0F, 3.0F, 1.0F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(backleg2R, -0.3944444176086212F, 1.064650843716541F, 0.07888888185724856F);
        this.bubble = new ModelRenderer(this, 0, 20);
        this.bubble.setRotationPoint(0.0F, 2.5F, -2.3F);
        this.bubble.addBox(-1.5F, -1.5F, -1.5F, 3.0F, 3.0F, 3.0F, 0.0F, 0.0F, 0.3F);
        this.body = new ModelRenderer(this, 0, 0);
        this.body.setRotationPoint(0.0F, 20.8F, 0.0F);
        this.body.addBox(-2.0F, -1.5F, -2.5F, 4.0F, 3.0F, 5.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(body, -0.47333330778824817F, 0.017453292519943295F, 0.0F);
        this.backleg1R = new ModelRenderer(this, 10, 10);
        this.backleg1R.setRotationPoint(-2.0F, 0.0F, 0.8F);
        this.backleg1R.addBox(-3.0F, -0.5F, -1.0F, 3.0F, 1.5F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(backleg1R, 0.23666665389412408F, 0.7096508711977333F, -0.5916666430576886F);
        this.backleg1L = new ModelRenderer(this, 10, 10);
        this.backleg1L.setRotationPoint(2.0F, 0.0F, 0.8F);
        this.backleg1L.addBox(0.0F, -0.5F, -1.0F, 3.0F, 1.5F, 2.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(backleg1L, 0.23666665389412408F, -0.7096508711977333F, 0.5916666430576886F);
        this.eyeL = new ModelRenderer(this, 10, 15);
        this.eyeL.setRotationPoint(1.6F, -0.9F, -2.4F);
        this.eyeL.addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
        this.setRotateAngle(eyeL, 0.3549999725188077F, 0.1972222088043106F, -0.43039819087864056F);
        this.backleg1L.addChild(this.backleg2L);
        this.body.addChild(this.head);
        this.body.addChild(this.frontlegR);
        this.head.addChild(this.eyeR);
        this.body.addChild(this.frontlegL);
        this.backleg1R.addChild(this.backleg2R);
        this.head.addChild(this.bubble);
        this.body.addChild(this.backleg1R);
        this.body.addChild(this.backleg1L);
        this.head.addChild(this.eyeL);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) { 
        ImmutableList.of(this.body).forEach((modelRenderer) -> { 
            modelRenderer.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    @Override
    public void setRotationAngles(ToadEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {}

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
