package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.ColubridEntity;
import com.ukan.loc.entity.models.ColubridModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ColubridRender extends MobRenderer<ColubridEntity, ColubridModel>{
	
	private static final ResourceLocation DOLICHOPHIS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_dolichophis.png");
	private static final ResourceLocation ELAPHE = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_elaphe.png");
	private static final ResourceLocation ELAPHE2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_elaphe2.png");
	private static final ResourceLocation MALPOLON_VOLGIVAGUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_malpolon_volgivagus.png");
	private static final ResourceLocation PAPILIO = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_natrix_papiliocephalus.png");
	private static final ResourceLocation PAPILIO2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_natrix_papiliocephalus2.png");
	private static final ResourceLocation PISCIPHAGUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_natrix_pisciphagus.png");
	private static final ResourceLocation PISCIPHAGUS2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_natrix_pisciphagus2.png");
	private static final ResourceLocation PISCIPHAGUS3 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_natrix_pisciphagus3.png");
	private static final ResourceLocation PERSONATUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_platyceps_personatus.png");
	private static final ResourceLocation VULGARIS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_platyceps_vulgaris.png");
	private static final ResourceLocation TESSELLATUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_telescopus_tessellatus.png");
	private static final ResourceLocation TESSELLATUS2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_telescopus_tessellatus2.png");
	private static final ResourceLocation VIPERAMIMUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_viperamimus.png");
	private static final ResourceLocation VIPERAMIMUS2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_viperamimus2.png");
	private static final ResourceLocation VIPERAMIMUS3 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_viperamimus3.png");
	private static final ResourceLocation HOMINIPHILUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_zamenis_hominiphilus.png");
	private static final ResourceLocation HOMINIPHILUS2 = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_zamenis_hominiphilus2.png");
	private static final ResourceLocation REGIUS = new ResourceLocation(LOCMain.MODID, "textures/entity/colubrid_zamenis_regius.png");
	
	

	public ColubridRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new ColubridModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(ColubridEntity entity) {
		switch(entity.getBool()) {
		case 0:
		default:
			return DOLICHOPHIS;
		case 1:
			return ELAPHE;
		case 2:
			return ELAPHE2;
		case 3:
			return MALPOLON_VOLGIVAGUS;
		case 4:
			return PAPILIO;
		case 5:
			return PAPILIO2;
		case 6:
			return PISCIPHAGUS;
		case 7:
			return PISCIPHAGUS2;
		case 8:
			return PISCIPHAGUS3;
		case 9:
			return PERSONATUS;
		case 10:
			return VULGARIS;
		case 11:
			return TESSELLATUS;
		case 12:
			return TESSELLATUS2;
		case 13:
			return VIPERAMIMUS;
		case 14:
			return VIPERAMIMUS2;
		case 15:
			return VIPERAMIMUS3;
		case 16:
			return HOMINIPHILUS;
		case 17:
			return HOMINIPHILUS2;
		case 18:
			return REGIUS;
		}
		
	}
	
}
