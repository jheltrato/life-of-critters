package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.ToadEntity;
import com.ukan.loc.entity.models.ToadModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ToadRender extends MobRenderer<ToadEntity, ToadModel>{

	public ToadRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new ToadModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(ToadEntity entity) {
		return new ResourceLocation(LOCMain.MODID, "textures/entity/toad_bombina_inopinus.png");
	}

}
