package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.LacertidEntity;
import com.ukan.loc.entity.models.LacertidModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LacertidRender extends MobRenderer<LacertidEntity, LacertidModel>{

	public LacertidRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new LacertidModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(LacertidEntity entity) {
		return new ResourceLocation(LOCMain.MODID, "textures/entity/lacertid_flavamaculosus_f.png");
	}

}
