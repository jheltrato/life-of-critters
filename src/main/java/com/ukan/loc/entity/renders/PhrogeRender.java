package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.PhrogeEntity;
import com.ukan.loc.entity.models.PhrogeModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PhrogeRender extends MobRenderer<PhrogeEntity, PhrogeModel>{

	public PhrogeRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new PhrogeModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(PhrogeEntity entity) {
		return new ResourceLocation(LOCMain.MODID, "textures/entity/phroge_pelophylax_viridiventra.png");
	}

}
