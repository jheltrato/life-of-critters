package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.LeglessLizardEntity;
import com.ukan.loc.entity.models.LeglessLizardModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LeglessLizardRender extends MobRenderer<LeglessLizardEntity, LeglessLizardModel>{

	public LeglessLizardRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new LeglessLizardModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(LeglessLizardEntity entity) {
		return new ResourceLocation(LOCMain.MODID, "textures/entity/leglesslizard_anguis_aeridorsum.png");
	}

}
