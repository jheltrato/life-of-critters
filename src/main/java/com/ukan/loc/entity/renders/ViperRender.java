package com.ukan.loc.entity.renders;

import com.ukan.loc.LOCMain;
import com.ukan.loc.entity.critters.ViperEntity;
import com.ukan.loc.entity.models.ViperModel;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ViperRender extends MobRenderer<ViperEntity, ViperModel>{

	private static final ResourceLocation ARTICA1 = new ResourceLocation(LOCMain.MODID, "textures/entity/viper_artica.png");
	private static final ResourceLocation ARTICA2 = new ResourceLocation(LOCMain.MODID, "textures/entity/viper_artica2.png");
	private static final ResourceLocation VIRIDIS = new ResourceLocation(LOCMain.MODID, "textures/entity/viper_viridis.png");
	private static final ResourceLocation VIRIDIS2 = new ResourceLocation(LOCMain.MODID, "textures/entity/viper_viridis2.png");
	
	public ViperRender(EntityRendererManager renderManagerIn,float shadowSize) {
		super(renderManagerIn, new ViperModel(), shadowSize);
	}

	@Override
	public ResourceLocation getEntityTexture(ViperEntity entity) {
		switch(entity.getBool()) {
		case 0:
		default:
			return ARTICA1;
		case 1:
		return ARTICA2;
		case 2:
		return VIRIDIS;
		case 3:
		return VIRIDIS2;
		}
	}

}
