package com.ukan.loc.entity;

import java.util.Random;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public abstract class CritterEntity extends AnimalEntity {

	public CritterEntity(EntityType<? extends AnimalEntity> type, World worldIn) {
		super(type, worldIn);
		this.experienceValue = 25;

	}

	@Override
	protected void registerData() {
		super.registerData();
	}
	
	@Override
	protected void registerAttributes() {
		super.registerAttributes();
		this.getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
	}
	
	@Override
	protected boolean isDespawnPeaceful() {
		return false;
	}
	
	
	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}


	
	public static boolean func_223364_b(EntityType<? extends CritterEntity> animal, IWorld worldIn, SpawnReason reason, BlockPos pos, Random random) {
		return animal.func_225437_d() && worldIn.getLightSubtracted(pos, 0) > 8;
	}


	
}
