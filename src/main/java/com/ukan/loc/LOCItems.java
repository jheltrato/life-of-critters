package com.ukan.loc;

import com.ukan.loc.item.SpawnerEggItem;

import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class LOCItems {

	
	public static DeferredRegister<Item> MOD_ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, LOCMain.MODID);
	
	@SuppressWarnings("unused")
	private static RegistryObject<Item> addItemEggSpawner (String name, EntityType<?> entityType, int primaryColor, int secondaryColor ) {
		return MOD_ITEMS.register(name, () -> new SpawnerEggItem(entityType, primaryColor, secondaryColor));
	}

}
