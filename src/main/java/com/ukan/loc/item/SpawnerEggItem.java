package com.ukan.loc.item;


import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;

public class SpawnerEggItem extends SpawnEggItem {

	public SpawnerEggItem(EntityType<?> typeIn, int primaryColorIn, int secondaryColorIn) {
		super(typeIn, primaryColorIn, secondaryColorIn, new Item.Properties().group(ItemGroup.MISC));
	}

}
