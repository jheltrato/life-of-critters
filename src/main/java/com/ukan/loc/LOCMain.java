package com.ukan.loc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(LOCMain.MODID)// LOCMain.MOD <-- Because the variable is static.
public class LOCMain
{
	public static final String MODID = "loc"; // Why we set our modid as a variable? A: It's because this will be use in many forge server features in the future.
    // Directly reference a log4j logger. A: just an access for a debugger!
    private static final Logger LOGGER = LogManager.getLogger();
    

    public LOCMain() {
    	//The Event Bus API whenever you made a feature it is processed here to make the Minecraft know.
    	IEventBus event = FMLJavaModLoadingContext.get().getModEventBus();
    	
    	event.addListener(this::clientSetupEvent);
    	event.addListener(this::commonSetupEvent);
        // Applied the method #deferredRegisterEvent in the constructor
        this.deferredRegisterEvent(event);
        MinecraftForge.EVENT_BUS.register(this);
    }
    
    //Deferred Register - This is a method that includes all additions of server features eg. Items/Blocks/Fluids/Entities/ etc.
    private void deferredRegisterEvent(final IEventBus eventBus) {
    	// To ensure that function is fired i added this 2 lines below so you can see them in the console.
    	LOGGER.info(LOCMain.MODID.toUpperCase() + " " + "deferred objects into the game using ForgeRegistries.");
    	System.out.println(LOCMain.MODID.toUpperCase() + " " + "deferred objects into the game using ForgeRegistries.");
    	
    	LOCEntities.LOCENTITIES.register(eventBus);
    	

    }
    
    @SuppressWarnings("deprecation")
	@OnlyIn(Dist.CLIENT)
    private void clientSetupEvent(final FMLClientSetupEvent event) {
    	DistExecutor.runWhenOn(Dist.CLIENT, () -> LOCEntities::renderingRegistry);
    }
    
    private void commonSetupEvent(final FMLCommonSetupEvent event) {
    	LOCEntities.entitySpawnPlacementFunction();
    }
    
    
    
    

}
