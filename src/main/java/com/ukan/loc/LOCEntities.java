package com.ukan.loc;

import com.ukan.loc.entity.critters.ColubridEntity;
import com.ukan.loc.entity.critters.LacertidEntity;
import com.ukan.loc.entity.critters.LeglessLizardEntity;
import com.ukan.loc.entity.critters.PhrogeEntity;
import com.ukan.loc.entity.critters.ToadEntity;
import com.ukan.loc.entity.critters.ViperEntity;
import com.ukan.loc.entity.renders.ColubridRender;
import com.ukan.loc.entity.renders.LacertidRender;
import com.ukan.loc.entity.renders.LeglessLizardRender;
import com.ukan.loc.entity.renders.PhrogeRender;
import com.ukan.loc.entity.renders.ToadRender;
import com.ukan.loc.entity.renders.ViperRender;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntitySpawnPlacementRegistry.PlacementType;
import net.minecraft.entity.EntityType;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class LOCEntities {


	
	public static final DeferredRegister<EntityType<?>> LOCENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, LOCMain.MODID);
	
	public static final EntityType<ColubridEntity> colubrid_entity = EntityType.Builder.create(ColubridEntity::new, EntityClassification.CREATURE).size(0.3F, 0.2F).build("colubrid");
	public static final EntityType<LacertidEntity> lacertid_entity = EntityType.Builder.create(LacertidEntity::new, EntityClassification.CREATURE).size(0.2F, 0.2F).build("lacertid");
	public static final EntityType<LeglessLizardEntity> leglesslizard_entity = EntityType.Builder.create(LeglessLizardEntity::new, EntityClassification.CREATURE).size(0.2F, 0.2F).build("leglesslizard");
	public static final EntityType<PhrogeEntity> phroge_entity = EntityType.Builder.create(PhrogeEntity::new, EntityClassification.CREATURE).size(0.3F, 0.3F).build("phroge");
	public static final EntityType<ToadEntity> toad_entity = EntityType.Builder.create(ToadEntity::new, EntityClassification.CREATURE).size(0.3F, 0.3F).build("toad");
	public static final EntityType<ViperEntity> viper_entity = EntityType.Builder.create(ViperEntity::new, EntityClassification.CREATURE).size(0.4F, 0.2F).build("viper");
	
	public static final RegistryObject<EntityType<ColubridEntity>> colubrid = LOCENTITIES.register("colubrid", () -> colubrid_entity); 
	public static final RegistryObject<EntityType<LacertidEntity>> lacertid = LOCENTITIES.register("lacertid", () -> lacertid_entity); 
	public static final RegistryObject<EntityType<LeglessLizardEntity>> leglesslizard = LOCENTITIES.register("leglesslizard", () -> leglesslizard_entity); 
	public static final RegistryObject<EntityType<PhrogeEntity>> phroge = LOCENTITIES.register("phroge", () -> phroge_entity); 
	public static final RegistryObject<EntityType<ToadEntity>> toad = LOCENTITIES.register("toad", () -> toad_entity); 
	public static final RegistryObject<EntityType<ViperEntity>> viper = LOCENTITIES.register("viper", () -> viper_entity); 
	
	
	public static void entitySpawnPlacementFunction() {
		EntitySpawnPlacementRegistry.register(colubrid.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, ColubridEntity::func_223364_b);
		EntitySpawnPlacementRegistry.register(lacertid.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, LacertidEntity::func_223364_b);
		EntitySpawnPlacementRegistry.register(leglesslizard.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, LeglessLizardEntity::func_223364_b);
		EntitySpawnPlacementRegistry.register(phroge.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, PhrogeEntity::func_223364_b);
		EntitySpawnPlacementRegistry.register(toad.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, ToadEntity::func_223364_b);
		EntitySpawnPlacementRegistry.register(viper.get(), PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, ViperEntity::func_223364_b);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void renderingRegistry() {
		RenderingRegistry.registerEntityRenderingHandler(colubrid.get(), m -> new ColubridRender(m, 0.02F));
		RenderingRegistry.registerEntityRenderingHandler(lacertid.get(), m -> new LacertidRender(m, 0.02F));
		RenderingRegistry.registerEntityRenderingHandler(leglesslizard.get(), m -> new LeglessLizardRender(m, 0.02F));
		RenderingRegistry.registerEntityRenderingHandler(phroge.get(), m -> new PhrogeRender(m, 0.02F));
		RenderingRegistry.registerEntityRenderingHandler(toad.get(), m -> new ToadRender(m, 0.02F));
		RenderingRegistry.registerEntityRenderingHandler(viper.get(), m -> new ViperRender(m, 0.02F));
	}
}
